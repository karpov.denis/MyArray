class MyArray {
  constructor(...arg) {
    Object.defineProperty(this, '_length', {
      enumerable: false,
      writable: true,
      configurable: false
    });

    switch (arg.length) {
    case 0:
      this._length = 0;
      break;

    case 1:
      if (typeof arg[0] !== 'number' || Number.isNaN(arg[0])) {
        throw new Error('CreationError: invalid length value');
      } else {
        this._length = arg[0];
      }
      break;

    default:
      this._length = arg.length;

      for (let i = 0; i < this._length; i++) {
        this[i] = arg[i];
      }
      break;
    }
  }

  set length(value) {
    if (typeof value !== 'number' || Number.isNaN(value)) {
      throw new Error('CreationError: invalid length value');
    }

    if (this._length > value) {
      for (let i = value; i < this._length; i++) {
        delete this[i];
      }
    }

    this._length = value;
  }

  get length() {
    return this._length;
  }

  [Symbol.iterator]() {
    let index = 0;
    const that = this;

    return {
      next() {
        if (index < that.length) {
          index += 1;
          return {
            value: that[index - 1],
            done: false
          };
        } else {
          return {
            done: true
          };
        }
      }
    };
  }

  push(...arg) {
    for (let i = 0; i < arg.length; i++) {
      this[i + this.length] = arg[i];
    }
    this.length = arg.length + this.length;
    return this.length;
  }

  pop() {
    if (this.length === 0) {
      return undefined;
    }

    const lastEl = this[this.length - 1];
    this.length = this.length - 1;
    return lastEl;
  }

  unshift(...arg) {
    const currentLen = this.length;

    for (let i = 0; i < arg.length; i++) {
      this[i + this.length] = this[i];
      this[i] = arg[i];
    }
    this.length = arg.length + this.length;
    return this.length;
  }

  shift() {
    if (this.length === 0) {
      return undefined;
    }

    for (let i = 1; i < this.length; i++) {
      this[i - 1] = this[i];
    }

    const firstEl = this[0];
    this.length = this.length - 1;
    return firstEl;
  }

  static from(arrLike, mapFn, thisArg) {
    if (arrLike.length === undefined) {
      return new MyArray();
    }

    // if (
    //   arguments[1] !== undefined &&
    //   (typeof mapFn !== 'function' ||
    //     Object.prototype.toString.call(mapFn) !== '[object Function]')
    // ) {
    //   throw new TypeError(
    //     'myArr.from: when provided, the second argument must be a function'
    //   );
    // }

    const result = new MyArray();
    result.length = arrLike.length;

    for (let i = 0; i < arrLike.length; i++) {
      mapFn
        ? (result[i] = mapFn.call(thisArg, arrLike[i]))
        : (result[i] = arrLike[i]);
    }

    return result;
  }

  forEach(callback, thisArg) {
    // if (
    //   typeof callback !== 'function' ||
    //   Object.prototype.toString.call(mapFn) !== '[object Function]'
    // ) {
    //   throw new TypeError(`${callback} is not a function`);
    // }

    for (let i = 0; i < this.length; i++) {
      if (i in this) {
        callback.call(thisArg, this[i], i, this);
      }
    }
  }

  map(callback, thisArg) {
    // if (
    //   typeof callback !== 'function' ||
    //   Object.prototype.toString.call(mapFn) !== '[object Function]'
    // ) {
    //   throw new TypeError(`${callback} is not a function`);
    // }

    const result = new MyArray();
    result.length = this.length;

    for (let i = 0; i < this.length; i++) {
      if (i in this) {
        result[i] = callback.call(thisArg, this[i], i, this);
      }
    }
    return result;
  }

  reduce(callback, startValue) {
    // if (
    //   typeof callback !== 'function' ||
    //   Object.prototype.toString.call(callback) !== '[object Function]'
    // ) {
    //   throw new TypeError(`${callback} is not a function`);
    // }

    let i = 0;
    let result = startValue;

    startValue ? (i = 0) : ((i = 1), (result = this[0]));

    for (i; i < this.length; i++) {
      result = callback(result, this[i], i, this);
    }
    return result;
  }

  filter(callback, thisArg) {
    if (
      typeof callback !== 'function' ||
      Object.prototype.toString.call(callback) !== '[object Function]'
    ) {
      throw new TypeError(`${callback} is not a function`);
    }

    const result = new MyArray();

    for (let i = 0; i < this.length; i++) {
      if (callback.call(thisArg, this[i], i, this)) {
        result[i] = this[i];
        result.length += 1;
      }
    }
    return result;
  }

  sort(...arg) {
    const result = this;
    let helpVal = 0;

    if (
      arg[0] !== undefined &&
      (typeof arg[0] !== 'function' ||
        Object.prototype.toString.call(arg[0]) !== '[object Function]')
    ) {
      for (let i = 0; i < this.length - 1; i++) {
        for (let j = 0; j < result.length - i; j++) {
          if (arg[0](this[j], this[j + 1]) > 0) {
            helpVal = this[j];
            this[j] = this[j + 1];
            this[j + 1] = helpVal;
          }
        }
      }
      return result;
    }

    for (let i = 0; i < result.length - 1; i++) {
      for (let j = 0; j < result.length - i; j++) {
        if (String(result[j]) > String(result[j + 1])) {
          helpVal = this[j];
          this[j] = this[j + 1];
          this[j + 1] = helpVal;
        }
      }
    }

    return result;
  }

  toString() {
    let str = '';

    for (let i = 0; i < this.length; i++) {
      i === this.length - 1 ? (str += this[i]) : (str = `${str + this[i]},`);
    }

    return str;
  }
}
export default MyArray;
